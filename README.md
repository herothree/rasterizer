This repo implements a rasterizer from scratch, using the scanline algorithm to process triangles
and draw them on the screen, taking into account transforms, a z buffer, and lots of linear interpolation.

It is currently set up to be built on OS X using CMake. 

See the files:  
 * jsigrist0.png  
 * jsigrist250.png  
 * jsigrist500.png  
 * jsigrist750.png  
For a sample of the output produced by the program.

The file project1F.cxx contains the relevant code.