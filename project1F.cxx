/*
 * Jeremy Sigrist - CIS 541
 * This program draws triangles on an image.
 */

// Gives different levels of debugging information
// Higher -> more information
#define DEBUG 0

#if DEBUG==0
// For assert
#define NDEBUG
#endif

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vtkCellArray.h>
#include <vtkDataSet.h>
#include <vtkDataSetWriter.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkImageData.h>
#include <vtkPNGWriter.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkUnsignedCharArray.h>

// How wide should the output image be?
#define WIDTH (1000)

// How tall should the output image be?
#define HEIGHT (1000)

// How many points are in a triangle?
#define POINTS_IN_TRIANGLE (3)

// What dimensional space are we working in?
#define DIMENSIONS (3)

// How many bytes are used to store a pixel?
#define BYTES_PER_PIXEL (1)

// What should the output filename be? .png will be automatically appended to this.
#define OUTPUT_FILENAME ("jsigrist")

typedef enum
{
    RED_INDEX=0,
    GREEN_INDEX,
    BLUE_INDEX,
    NUM_COLORS
} colorIndice_t;

double ceil441(double f)
{
    return ceil(f-0.00001);
}

double floor441(double f)
{
    return floor(f+0.00001);
}

/*
 * Linear interpolate a value between two points
 * @param startLocX - The X coordinate for the first point
 * @param startLocY - The Y coordinate for the first point
 * @param endLocX - The X coordinate for the second point
 * @param endLocY - The Y coordinate for the second point
 * @param currentLocX - The X coordinate for the point to interpolate to
 * @param currentLocY - The Y coordinate for the point to interpolate to
 * @param startVal - The value to interpolate from at the starting point
 * @param endVal - The value to interpolate from at the ending point
 */
double lerp(double startLocX, double startLocY, double endLocX, double endLocY, double currentLocX, 
        double currentLocY, double startVal, double endVal)
{
#if DEBUG > 4
    printf("Lerp: start: (%.3f, %.3f), end: (%.3f, %.3f), current: (%.3f, %.3f), startVal: %.3f, endVal: %.3f\n", startLocX, startLocY, endLocX, endLocY, currentLocX, 
            currentLocY, startVal, endVal);
#endif
    if (((startLocX == endLocX) && (startLocY == endLocY)))
    {
#if DEBUG > 3
        printf("Interpolating from one point (%.4f, %.4f) to itself.\n", startLocX, startLocY);
#endif
        return startVal;
    }

    // If the x values are different, interpolate on them
    // This is needed for interpolating between points like (1, 4) and (1, 8)
    if (startLocX != endLocX)
    {
        return startVal + ((currentLocX - startLocX)/(endLocX - startLocX)) * (endVal - startVal);
    }
    else // The x values are the same
    {
        // Interpolate on the Y values instead
        return startVal + ((currentLocY - startLocY)/(endLocY - startLocY)) * (endVal - startVal);
    }
}

/*
 * Multiplies a vector by a scalar
 * @param s - the scalar
 * @param v - the vector
 */
void scalarMul(double s, double v[])
{
    for (int i = 0; i < DIMENSIONS; ++i)
    {
        v[i] *= s;
    }
}

/*
 * Scales the vector make it a unit vector
 * @param v - the vector
 */
void normalize(double v[])
{
    double norm = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);

    scalarMul(1/norm, v);
}

/*
 * Copies the values from one vector to another
 * @param src - the source vector
 * @param dest - the destination vector
 */
void copyVector(double src[], double dest[])
{
    for (int i = 0; i < DIMENSIONS; ++i)
    {
        dest[i] = src[i];
    }
}

/*
 * Calculates the dot product of two vectors
 * @param v1 - the first vector
 * @param v2 - the second vector
 */
double dot(double v1[], double v2[])
{
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

/*
 * Calculates the cross produce between two vectors
 * @param v1 - the first vector
 * @param v2 - the second vector
 * @param result - the vector to store the result in. Can be v1 or v2
 */
void cross(double v1[], double v2[], double result[])
{
    assert(DIMENSIONS == 3); // Cross product is only defined in R3

    // Use a temp vector in case result is v1 or v2
    double tmp[3]; 
    tmp[0] = v1[1] * v2[2] - v1[2] * v2[1];
    tmp[1] = v1[2] * v2[0] - v1[0] * v2[2];
    tmp[2] = v1[0] * v2[1] - v1[1] * v2[0];

    copyVector(tmp, result);
}

/*
 * Subtracts one vector from another
 * @param v1 - the first vector
 * @param v2 - the second vector
 */
void subtract(double v1[], double v2[], double result[])
{
    // Subtracts v2 from v1
    for (int i = 0; i < DIMENSIONS; ++i)
    {
        result[i] = v1[i] - v2[i];
    }
}

/*
 * A struct containing some parameters to influence the lighting of the image
 */
struct LightingParameters
{
    LightingParameters(void)
    {
        lightDir[0] = -0.6;
        lightDir[1] = 0;
        lightDir[2] = -0.8;
        Ka = 0.3;
        Kd = 0.7;
        Ks = 5.3;
        alpha = 7.5;
    };

    double lightDir[DIMENSIONS]; // The direction of the light source
    double Ka;           // The coefficient for ambient lighting.
    double Kd;           // The coefficient for diffuse lighting.
    double Ks;           // The coefficient for specular lighting.
    double alpha;        // The exponent term for specular lighting.
};

LightingParameters lp;

/*
 * A class that represents a matrix
 *
 * Supports multiplication by another matrix or transforming points
 */
class Matrix
{
  public:
      // Use homogeneous coordinates 
    double          A[DIMENSIONS + 1][DIMENSIONS + 1];

    // Implemented below
    void            TransformPoint(const double *ptIn, double *ptOut);
    static Matrix   ComposeMatrices(const Matrix &, const Matrix &);
    void            Print(ostream &o);
};

/*
 * A class that represents a camera location for the image
 *
 */
class Camera
{

  public:
    double near; // Distance of the near plane
    double far; // Distance of the far plane
    double angle; // Viewing angle of the frustum
    double position[DIMENSIONS]; // The position of the camera in world space
    double focus[DIMENSIONS]; // The focus direction of the camera
    double up[DIMENSIONS]; // What direction (in world space) is up?

    /*
     * Fills up a matrix representing the view transform
     * from camera space to image space
     */
    void ViewTransform(Matrix& result)
    {
        
        result.A[0][0] = cos(angle / 2) / sin(angle / 2);
        result.A[0][1] = 0;
        result.A[0][2] = 0;
        result.A[0][3] = 0;

        result.A[1][0] = 0;
        result.A[1][1] = cos(angle / 2) / sin(angle / 2);
        result.A[1][2] = 0;
        result.A[1][3] = 0;

        result.A[2][0] = 0;
        result.A[2][1] = 0;
        result.A[2][2] = (far + near) / (far - near);
        result.A[2][3] = -1;

        result.A[3][0] = 0;
        result.A[3][1] = 0;
        result.A[3][2] = (2 * far * near) / (far - near);
        result.A[3][3] = 0;
    }

    /*
     * Fills up a matrix representing the camera transform
     * from world space to camera space
     */
    void CameraTransform(Matrix& result)
    {
        // O = camera position
        
        // v1 = Up X (O - focus)
        double v1[DIMENSIONS];
        copyVector(this->position, v1);
        subtract(v1, this->focus, v1);
        cross(this->up, v1, v1);
        normalize(v1);

        // v2 = (O - focus) X v1
        double v2[DIMENSIONS];
        copyVector(this->position, v2);
        subtract(v2, this->focus, v2);
        cross(v2, v1, v2);
        normalize(v2);

        // v3 = (O - focus)
        double v3[DIMENSIONS];
        copyVector(this->position, v3);
        subtract(v3, this->focus, v3);
        normalize(v3);

        // t = <0,0,0> - O
        double t[DIMENSIONS];
        copyVector(this->position, t);
        scalarMul(-1, t);
        
        result.A[0][0] = v1[0];
        result.A[0][1] = v2[0];
        result.A[0][2] = v3[0];
        result.A[0][3] = 0;

        result.A[1][0] = v1[1];
        result.A[1][1] = v2[1];
        result.A[1][2] = v3[1];
        result.A[1][3] = 0;

        result.A[2][0] = v1[2];
        result.A[2][1] = v2[2];
        result.A[2][2] = v3[2];
        result.A[2][3] = 0;

        result.A[3][0] = dot(v1, t);
        result.A[3][1] = dot(v2, t);
        result.A[3][2] = dot(v3, t);
        result.A[3][3] = 1;
    }

    /*
     * Fills up a matrix representing the device transform
     * from image space to device space
     */
    void DeviceTransform(Matrix& result)
    {

        double W = std::max(HEIGHT, WIDTH) / 2;
        
        result.A[0][0] = W;
        result.A[0][1] = 0;
        result.A[0][2] = 0;
        result.A[0][3] = 0;

        result.A[1][0] = 0;
        result.A[1][1] = W;
        result.A[1][2] = 0;
        result.A[1][3] = 0;

        result.A[2][0] = 0;
        result.A[2][1] = 0;
        result.A[2][2] = 1;
        result.A[2][3] = 0;

        result.A[3][0] = W;
        result.A[3][1] = W;
        result.A[3][2] = 0;
        result.A[3][3] = 1;
    }
};

/*
 * This class stores data to represent a Triangle
 */
class Triangle
{
    public:

        // Convention: The two points with the same y-coordinate will be at indices 0 and 2, 
        // with the triangle at 0 having the smaller y coordinate.
        // The sortByY method breaks this convention. The arrange method restores it.
        double X[POINTS_IN_TRIANGLE];
        double Y[POINTS_IN_TRIANGLE];
        double Z[POINTS_IN_TRIANGLE];
        double colors[POINTS_IN_TRIANGLE][NUM_COLORS];
        double normals[POINTS_IN_TRIANGLE][DIMENSIONS];
        double shading[POINTS_IN_TRIANGLE];

        /*
         * The constructor for the triangle. Will copy data from the
         * passed in triangle if applicable.
         *
         * @param tri - the triangle to copy data from, or NULL if omitted
         */
        Triangle(Triangle* tri = NULL)
        {
            arranged = false;
           
            if (NULL != tri)
            {
                for (int i = 0; i < POINTS_IN_TRIANGLE; ++i)
                {
                    this->copyPoint(i, (*tri), i);
                }
            }
        }

        /*
         * Returns the maximum Y value in the triangle.
         */ 
        double maxY()
        {
            double result = Y[0];
            if (Y[1] > result) result = Y[1];
            if (Y[2] > result) result = Y[2];
            return result;
        }

        /*
         * Returns the minimum Y value in the triangle.
         */ 
        double minY()
        {
            double result = Y[0];
            if (Y[1] < result) result = Y[1];
            if (Y[2] < result) result = Y[2];
            return result;
        }

        /*
         * Returns true if the triangle has a flat top, and
         * false if the triangle has a flat bottom. 
         */
        bool isFlatTop()
        {
            if (!arranged)
            {
                this->arrange();
            }

            // Check if the middle point is higher than the leftmost one
            if ((Y[0] == Y[2] && Y[0] < Y[1])
                    || (Y[0] == Y[1] && Y[0] < Y[2])
                    || (Y[1] == Y[2] && Y[1] < Y[0])
               ) 
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /*
         * Rearranges the points in the triangle to match the convention listed above.
         */
        void arrange()
        {
            double temp;

            if (Y[0] == Y[1])
            {
                // Swap 1 and 2
                swapPoints(1, 2);
            }

            if (Y[1] == Y[2])
            {
                // Swap 0 and 1
                swapPoints(0, 1);
            }

            if (Y[0] == Y[2] && X[0] > X[2])
            {
                // Swap 0 and 2
                swapPoints(0, 2);
            }

            arranged = true;
        }

        /*
         * Rearranges the triangle so the points are sorted from low to high by Z value.
         */
        void sortByY()
        {
            double temp;

            if (Y[0] > Y[1])
            {
                swapPoints(0, 1);
            }

            if (Y[1] > Y[2])
            {
                swapPoints(1, 2);
            }

            if (Y[0] > Y[1])
            {
                swapPoints(0, 1);
            }
        }

        /*
         * Copys a point from another triangle into this one.
         */
        void copyPoint(int myIndex, Triangle& other, int otherIndex)
        {
            assert(myIndex < POINTS_IN_TRIANGLE);
            assert(myIndex >= 0);
            assert(otherIndex < POINTS_IN_TRIANGLE);
            assert(otherIndex >= 0);

            this->X[myIndex] = other.X[otherIndex];
            this->Y[myIndex] = other.Y[otherIndex];
            this->Z[myIndex] = other.Z[otherIndex];
            this->shading[myIndex] = other.shading[otherIndex];
            for (int i = 0; i < POINTS_IN_TRIANGLE; ++i)
            {
                this->colors[myIndex][i] = other.colors[otherIndex][i];
                this->normals[myIndex][i] = other.normals[otherIndex][i];
            }
        }

    private:

        // Remembers whether or not the points of the triangle are in order
        bool arranged;

        /*
         * Swaps the data stored at the two indices index1 and index2.
         */
        void swapPoints(int index1, int index2)
        {
            double tmp;

            tmp = X[index2];
            X[index2] = X[index1];
            X[index1] = tmp;

            tmp = Y[index2];
            Y[index2] = Y[index1];
            Y[index1] = tmp;

            tmp = Z[index2];
            Z[index2] = Z[index1];
            Z[index1] = tmp;

            tmp = shading[index2];
            shading[index2] = shading[index1];
            shading[index1] = tmp;

            for (int i = 0; i < NUM_COLORS; ++i)
            {
                tmp = colors[index1][i];
                colors[index1][i] = colors[index2][i];
                colors[index2][i] = tmp;
            }

            for (int i = 0; i < DIMENSIONS; ++i)
            {
                tmp = normals[index1][i];
                normals[index1][i] = normals[index2][i];
                normals[index2][i] = tmp;
            }
        }
};

/*
 * Represents the screen the images are displayed on
 */
class Screen
{
    public:
        vtkImageData *image;
        unsigned char *buffer;
        int width, height;

        // Goes from -1 to 0 for each pixel (camera is at 0)
        double *z_buffer;

        /*
         * Creates and returns a vtk image object.
         * @param width - How wide should the new image be?
         * @param height - How tall should the new image be?
         */
        vtkImageData * newImage(int width, int height)
        {
            vtkImageData *img = vtkImageData::New();
            img->SetDimensions(width, height, 1);
            img->AllocateScalars(VTK_UNSIGNED_CHAR, NUM_COLORS);

            return img;
        }

        /*
         * Constructor for the screen class. Creates a screen with the
         * given height and width
         */ 
        Screen(int width, int height)
        {
            this->width = width;
            this->height = height;

            image = this->newImage(this->width, this->height);
            buffer = (unsigned char *) image->GetScalarPointer(0,0,0);

            // Initialize the buffer to zero
            for (int i = 0 ; i < this->width * this->height * NUM_COLORS ; i++)
            {
                buffer[i] = 0;
            }

            // Initialize z-buffer to -1 (camera is at 0)
            z_buffer = new double[this->width * this->height];
            for (int i = 0; i < this->width * this->height; ++i)
            {
                z_buffer[i] = -1;
            }
        }

        /*
         * The destructor for the screen class
         */
        ~Screen()
        {
            delete this->z_buffer;
            this->image->Delete();
        }

        /*
         * Saves an image to a png file
         * @param img - The vtk image to save
         * @param filename - The filename to save the image to. 
         *                   ".png" is appended to the parameter.
         */
        void writeImage(const char *filename)
        {
            std::string full_filename = filename;
            full_filename += ".png";
            vtkPNGWriter *writer = vtkPNGWriter::New();
            writer->SetInputData(this->image);
            writer->SetFileName(full_filename.c_str());
            writer->Write();
            writer->Delete();
        }

        /*
         * Returns the color for the given x and y coordinates
         */
        unsigned char * getColor(int x, int y)
        {
            if (x < 0 || x >= width)
            {
#if DEBUG > 1
                fprintf(stdout, "x out of bounds (0-%d): %d\n", width, x);
#endif
                return NULL;
            }

            if (y < 0 || y >= height)
            {
#if DEBUG > 1
                fprintf(stdout, "y out of bounds (0-%d): %d\n", height, x);
#endif
                return NULL;
            }

            return buffer + BYTES_PER_PIXEL * NUM_COLORS * (width * y + x);
        }

        /*
         * Returns the value of the z buffer at the given x and y coordinates
         */
        double getZ(int x, int y)
        {
            if (x < 0 || x >= width)
            {
#if DEBUG > 1
                fprintf(stdout, "x out of bounds (0-%d): %d\n", width, x);
#endif
                return 1;
            }

            if (y < 0 || y >= height)
            {
#if DEBUG > 1
                fprintf(stdout, "y out of bounds (0-%d): %d\n", height, x);
#endif
                return 1;
            }

            // Three bytes per pixel
            return z_buffer[this->width * y + x];
        }

        /*
         * Sets the pixel at the given x and y to the given color and z value
         */
        void setPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b, double z)
        {
            if (x < 0 || x >= width)
            {
#if DEBUG > 1
                fprintf(stdout, "x out of bounds (0-%d): %d\n", width, x);
#endif
                return;
            }

            if (y < 0 || y >= height)
            {
#if DEBUG > 1
                fprintf(stdout, "y out of bounds (0-%d): %d\n", height, x);
#endif
                return;
            }

#if DEBUG > 3
            fprintf(stdout, "Setting pixel (%d, %d) to [%d,%d,%d]\n", x, y, (int)r, (int)g, (int)b);
#endif

            buffer[BYTES_PER_PIXEL * NUM_COLORS * (width * y + x) + RED_INDEX * BYTES_PER_PIXEL] = r;
            buffer[BYTES_PER_PIXEL * NUM_COLORS * (width * y + x) + GREEN_INDEX * BYTES_PER_PIXEL] = g;
            buffer[BYTES_PER_PIXEL * NUM_COLORS * (width * y + x) + BLUE_INDEX * BYTES_PER_PIXEL] = b;
            z_buffer[width * y + x] = z;
        }
};

/*
 * Uses trigonometry
 */
double SineParameterize(int curFrame, int nFrames, int ramp)
{
    int nNonRamp = nFrames-2*ramp;
    double height = 1./(nNonRamp + 4*ramp/M_PI);
    if (curFrame < ramp)
    {
        double factor = 2*height*ramp/M_PI;
        double eval = cos(M_PI/2*((double)curFrame)/ramp);
        return (1.-eval)*factor;
    }
    else if (curFrame > nFrames-ramp)
    {
        int amount_left = nFrames-curFrame;
        double factor = 2*height*ramp/M_PI;
        double eval =cos(M_PI/2*((double)amount_left/ramp));
        return 1. - (1-eval)*factor;
    }
    double amount_in_quad = ((double)curFrame-ramp);
    double quad_part = amount_in_quad*height;
    double curve_part = height*(2*ramp)/M_PI;
    return quad_part+curve_part;
}

/*
 * Prints a matrix to the given output stream
 */
void Matrix::Print(ostream &o)
{
    for (int i = 0 ; i < 4 ; i++)
    {
        char str[256];
        sprintf(str, "(%.7f %.7f %.7f %.7f)\n", A[i][0], A[i][1], A[i][2], A[i][3]);
        o << str;
    }
}

/*
 * Prints the values of a triangle object to stdout.
 */ 
void printTriangle(Triangle& t)
{
    printf("Points: (%.4f, %.4f, %.4f), (%.4f, %.4f, %.4f), (%.4f, %.4f, %.4f)\n", t.X[0], t.Y[0], t.Z[0], t.X[1], t.Y[1], t.Z[1], t.X[2], t.Y[2], t.Z[2]);
    printf("Colors: [%.3f, %.3f, %.3f], [%.3f, %.3f, %.3f], [%.3f, %.3f, %.3f]\n", t.colors[0][0], t.colors[0][1], t.colors[0][2], t.colors[1][0], t.colors[1][1], t.colors[1][2], t.colors[2][0], t.colors[2][1], t.colors[2][2]);
    printf("Normals: [%.3f, %.3f, %.3f], [%.3f, %.3f, %.3f], [%.3f, %.3f, %.3f]\n", t.normals[0][0], t.normals[0][1], t.normals[0][2], t.normals[1][0], t.normals[1][1], t.normals[1][2], t.normals[2][0], t.normals[2][1], t.normals[2][2]);
}

/*
 * Calculates some parameters for a given camera position
 */
Camera GetCamera(int frame, int nframes)
{
    double t = SineParameterize(frame, nframes, nframes/10);
    printf("t: %.10f\n", t);
    Camera c;
    c.near = 5;
    c.far = 200;
    c.angle = M_PI/6;
    c.position[0] = 40*sin(2*M_PI*t);
    c.position[1] = 40*cos(2*M_PI*t);
    c.position[2] = 40;
    c.focus[0] = 0;
    c.focus[1] = 0;
    c.focus[2] = 0;
    c.up[0] = 0;
    c.up[1] = 1;
    c.up[2] = 0;
    return c;
}

/*
 * Multiplies two matrices together and returns the result
 */
Matrix Matrix::ComposeMatrices(const Matrix &M1, const Matrix &M2)
{
    Matrix rv;
    for (int i = 0 ; i < 4 ; i++)
        for (int j = 0 ; j < 4 ; j++)
        {
            rv.A[i][j] = 0;
            for (int k = 0 ; k < 4 ; k++)
                rv.A[i][j] += M1.A[i][k]*M2.A[k][j];
        }

    return rv;
}

/*
 * Transforms a given point using the matrix
 */
void Matrix::TransformPoint(const double *ptIn, double *ptOut)
{
    ptOut[0] = ptIn[0]*A[0][0]
             + ptIn[1]*A[1][0]
             + ptIn[2]*A[2][0]
             + ptIn[3]*A[3][0];
    ptOut[1] = ptIn[0]*A[0][1]
             + ptIn[1]*A[1][1]
             + ptIn[2]*A[2][1]
             + ptIn[3]*A[3][1];
    ptOut[2] = ptIn[0]*A[0][2]
             + ptIn[1]*A[1][2]
             + ptIn[2]*A[2][2]
             + ptIn[3]*A[3][2];
    ptOut[3] = ptIn[0]*A[0][3]
             + ptIn[1]*A[1][3]
             + ptIn[2]*A[2][3]
             + ptIn[3]*A[3][3];
}

/*
 * Reads a vector of triangles out of a file.
 */
std::vector<Triangle> GetTriangles(void)
{
    vtkPolyDataReader *rdr = vtkPolyDataReader::New();
    rdr->SetFileName("proj1e_geometry.vtk");
    cerr << "Reading" << endl;
    rdr->Update();
    cerr << "Done reading" << endl;
    if (rdr->GetOutput()->GetNumberOfCells() == 0)
    {
        cerr << "Unable to open file!!" << endl;
        exit(EXIT_FAILURE);
    }
    vtkPolyData *pd = rdr->GetOutput();

    int numTris = pd->GetNumberOfCells();
    vtkPoints *pts = pd->GetPoints();
    vtkCellArray *cells = pd->GetPolys();
    vtkDoubleArray *var = (vtkDoubleArray *) pd->GetPointData()->GetArray("hardyglobal");
    double *color_ptr = var->GetPointer(0);
    vtkFloatArray *n = (vtkFloatArray *) pd->GetPointData()->GetNormals();
    float *normals = n->GetPointer(0);
    std::vector<Triangle> tris(numTris);
    vtkIdType npts;
    vtkIdType *ptIds;
    int idx;
    for (idx = 0, cells->InitTraversal() ; cells->GetNextCell(npts, ptIds) ; idx++)
    {
        if (npts != 3)
        {
            cerr << "Non-triangles!! ???" << endl;
            exit(EXIT_FAILURE);
        }
        double *pt = NULL;
        pt = pts->GetPoint(ptIds[0]);
        tris[idx].X[0] = pt[0];
        tris[idx].Y[0] = pt[1];
        tris[idx].Z[0] = pt[2];
        tris[idx].normals[0][0] = normals[3*ptIds[0]+0];
        tris[idx].normals[0][1] = normals[3*ptIds[0]+1];
        tris[idx].normals[0][2] = normals[3*ptIds[0]+2];
        pt = pts->GetPoint(ptIds[1]);
        tris[idx].X[1] = pt[0];
        tris[idx].Y[1] = pt[1];
        tris[idx].Z[1] = pt[2];
        tris[idx].normals[1][0] = normals[3*ptIds[1]+0];
        tris[idx].normals[1][1] = normals[3*ptIds[1]+1];
        tris[idx].normals[1][2] = normals[3*ptIds[1]+2];
        pt = pts->GetPoint(ptIds[2]);
        tris[idx].X[2] = pt[0];
        tris[idx].Y[2] = pt[1];
        tris[idx].Z[2] = pt[2];
        tris[idx].normals[2][0] = normals[3*ptIds[2]+0];
        tris[idx].normals[2][1] = normals[3*ptIds[2]+1];
        tris[idx].normals[2][2] = normals[3*ptIds[2]+2];

        // 1->2 interpolate between light blue, dark blue
        // 2->2.5 interpolate between dark blue, cyan
        // 2.5->3 interpolate between cyan, green
        // 3->3.5 interpolate between green, yellow
        // 3.5->4 interpolate between yellow, orange
        // 4->5 interpolate between orange, brick
        // 5->6 interpolate between brick, salmon
        double mins[7] = { 1, 2, 2.5, 3, 3.5, 4, 5 };
        double maxs[7] = { 2, 2.5, 3, 3.5, 4, 5, 6 };
        unsigned char RGB[8][3] = { { 71, 71, 219 },
                                    { 0, 0, 91 },
                                    { 0, 255, 255 },
                                    { 0, 128, 0 },
                                    { 255, 255, 0 },
                                    { 255, 96, 0 },
                                    { 107, 0, 0 },
                                    { 224, 76, 76 }
                                  };
        for (int j = 0 ; j < 3 ; j++)
        {
            float val = color_ptr[ptIds[j]];
            int r;
            for (r = 0 ; r < 7 ; r++)
            {
                if (mins[r] <= val && val < maxs[r])
                    break;
            }
            if (r == 7)
            {
                cerr << "Could not interpolate color for " << val << endl;
                exit(EXIT_FAILURE);
            }
            double proportion = (val-mins[r]) / (maxs[r]-mins[r]);
            tris[idx].colors[j][0] = (RGB[r][0]+proportion*(RGB[r+1][0]-RGB[r][0]))/255.0;
            tris[idx].colors[j][1] = (RGB[r][1]+proportion*(RGB[r+1][1]-RGB[r][1]))/255.0;
            tris[idx].colors[j][2] = (RGB[r][2]+proportion*(RGB[r+1][2]-RGB[r][2]))/255.0;
        }
    }

    return tris;
}

/*
 * Splits a triangle that does not have either a flat top or a flat bottom 
 * into two triangles, one with a flat top and one with a flat bottom.
 *
 * The triangle in place is modified, and another triangle is appended to the
 * end of the vector.
 *
 * tri1 will be modified to a flat top triangle, tri 2 should be passed in empty
 * and will be the flat bottom triangle.
 *
 * @param triangles A vector full of triangles
 * @param i The index in the vector of the triangle to split
 */
void splitTriangle(Triangle& tri1, Triangle& tri2)
{
    double slope;

    // Sort the points by their Y coordinates
    tri1.sortByY();

#if DEBUG > 2
    printf("Split triangle: \n");
    printTriangle(tri1);
#endif

    // Cut the triangle at the point with the middle Y coordinate

    // One of the points we need for our new triangle is the middle Y point
    tri2.copyPoint(0, tri1, 1);

    // The second point we need is the original top point for the triangle
    tri2.copyPoint(1, tri1, 2);

    // The third point will have the same Y coordinate as our first point
    tri2.Y[2] = tri2.Y[0];

    // And the x point will be calculated based on that y point:

    // Slope going from Y[0] to Y[2], or low to high.
    slope = (tri1.Y[2] - tri1.Y[0]) / (tri1.X[2] - tri1.X[0]);
    tri2.X[2] = tri1.X[0] + (tri2.Y[0] - tri1.Y[0]) / slope;

    // Interpolate between the highest and lowest points on the original triangle to get
    // the z value, the colors, and the normal vector
    tri2.Z[2] = lerp(tri1.X[0], tri1.Y[0], tri1.X[2], tri1.Y[2], tri2.X[2], tri2.Y[2], tri1.Z[0], tri1.Z[2]);
    tri2.shading[2] = lerp(tri1.X[0], tri1.Y[0], tri1.X[2], tri1.Y[2], tri2.X[2], tri2.Y[2], tri1.shading[0], tri1.shading[2]);

    for (int i = 0; i < NUM_COLORS; ++i)
    {
        tri2.colors[2][i] = lerp(tri1.X[0], tri1.Y[0], tri1.X[2], tri1.Y[2], tri2.X[2], tri2.Y[2], tri1.colors[0][i], tri1.colors[2][i]);
        tri2.normals[2][i] = lerp(tri1.X[0], tri1.Y[0], tri1.X[2], tri1.Y[2], tri2.X[2], tri2.Y[2], tri1.normals[0][i], tri1.normals[2][i]);
    }
    normalize(tri2.normals[2]);


    // Fix the original triangle by replacing its highest point with the 
    // new intersection point we just calculated for the new triangle
    tri1.copyPoint(2, tri2, 2);

#if DEBUG > 2
    printf("into tri1: \n");
    printTriangle(tri1);
    printf("and tri2:\n");
    printTriangle(tri2);
#endif
}

double calculateShading(LightingParameters& lightingParameters, double *viewDirection, double *normal)
{
    double shading = 0;

    double diffuse = 0;

    double specular = 0;
    double r[DIMENSIONS] = {0, 0, 0};

    double shininess = 1;
    double dp;

    //shading = Ka + Kd*Diffuse + Ks*Specular

    // Calculate the ambient component of the lighting as simply the ambient parameter
    shading += lightingParameters.Ka;

    // Calculate the diffuse component of the lighting as the dot produce of L and N
    // Use abs to achieve two sided lighting
    diffuse = std::abs(dot(normal, lightingParameters.lightDir));

#if DEBUG > 2
    printf("diffuse: %.3f\n", diffuse);
#endif

    shading += lightingParameters.Kd * diffuse;

    // Calculate the specular component of the lighting
    r[0] = normal[0];
    r[1] = normal[1];
    r[2] = normal[2];

    scalarMul(2 * dot(lightingParameters.lightDir, normal), r);
    r[0] -= lightingParameters.lightDir[0];
    r[1] -= lightingParameters.lightDir[1];
    r[2] -= lightingParameters.lightDir[2];

    // Make sure not to raise a negative dp to a power
    dp = dot(viewDirection, r);
    if (dp > 0)
    {
        specular = std::max(0.0, shininess * std::pow(dp, lightingParameters.alpha));
    }
    else
    {
        specular = 0.0;
    }

    shading += lightingParameters.Ks * specular;

#if DEBUG > 2
    printf("specular: %.3f\n", specular);
#endif

    return shading;
}

/*
 * Transforms each of the points in a triangle using a matrix
 */
void transformTriangle(Triangle& tri, Matrix& A)
{
    double ptIn[DIMENSIONS + 1];
    double ptOut[DIMENSIONS + 1];
    
    ptIn[DIMENSIONS] = 1;

    for (int i = 0; i < POINTS_IN_TRIANGLE; ++i)
    {
        // Set up the point
        ptIn[0] = tri.X[i];
        ptIn[1] = tri.Y[i];
        ptIn[2] = tri.Z[i];

        // Transform it
        A.TransformPoint(ptIn, ptOut);

        // Project back to w=1 line
        tri.X[i] = ptOut[0] / ptOut[3];
        tri.Y[i] = ptOut[1] / ptOut[3];
        tri.Z[i] = ptOut[2] / ptOut[3];
    }
}

/*
 * Runs the scanline algorithm on a single triangle.
 * @param tri - The triangle to render
 * @param m - The matrix to be used to transform the triangle depending
 *          on the camera angle
 * @param screen - The screen to write the rendered triangle to
 */
void scanTriangle(Triangle& tri, Matrix& m, double position[], Screen& screen)
{
    double minY = 0;
    double maxY = 0;
    double startX = 0;
    double endX = 0;
    double slopeFront = 0;
    double slopeBack = 0;
    double color[NUM_COLORS];
    double startXColor[NUM_COLORS];
    double endXColor[NUM_COLORS];
    double startZ = 0;
    double endZ = 0;
    double startShading = 0;
    double endShading = 0;
    double shading = 0;
    double z = 0;
    
    assert(tri.Y[0] == tri.Y[2]);

#if DEBUG > 0
    printf("1a: ");
    printTriangle(tri);
#endif



#if DEBUG > 0
    printf("1b: ");
    printTriangle(tri);
#endif

    // Get the minimum and maximum Y values from the triangle
    minY = tri.minY();
    maxY = tri.maxY();

#if DEBUG > 2
    printf("minY: %.3f, maxY: %.3f\n", minY, maxY);
#endif

    slopeFront = (tri.Y[1] - tri.Y[0]) / (tri.X[1] - tri.X[0]);
    slopeBack = (tri.Y[2] - tri.Y[1]) / (tri.X[2] - tri.X[1]);

#if DEBUG > 2
    printf("slopeFront: %.16f\n", slopeFront);
    printf("slopeBack: %.16f\n", slopeBack);
#endif

    // For each row
    for (int row = ceil441(minY); row <= floor441(maxY); ++row)
    {

        if (tri.isFlatTop())
        {
            // For flat top triangles, calculate the offset from 
            // the bottom middle point.
            startX = tri.X[1] + (row - minY) / slopeFront;
            endX = tri.X[1] + (row - minY) / slopeBack;
        }
        else
        {
            // For flat bottom triangles, calculate the offset from the 
            // left and right bottom points.
            startX = tri.X[0] + (row - minY) / slopeFront;
            endX = tri.X[2] + (row - minY) / slopeBack;
        }

        // Interpolate to find color at the ends
        for (int i = 0; i < NUM_COLORS; ++i)
        {
            startXColor[i] = lerp(tri.X[0], tri.Y[0], tri.X[1], tri.Y[1], startX, row, tri.colors[0][i], tri.colors[1][i]);
            endXColor[i] = lerp(tri.X[1], tri.Y[1], tri.X[2], tri.Y[2], endX, row, tri.colors[1][i], tri.colors[2][i]);
        }

        // Interpolate to find z value at left end
        startZ = lerp(tri.X[0], tri.Y[0], tri.X[1], tri.Y[1], startX, row, tri.Z[0], tri.Z[1]);

        // Interpolate to find z value at right end
        endZ = lerp(tri.X[1], tri.Y[1], tri.X[2], tri.Y[2], endX, row, tri.Z[1], tri.Z[2]);
        
        // Interpolate to find shading value at left end
        startShading = lerp(tri.X[0], tri.Y[0], tri.X[1], tri.Y[1], startX, row, tri.shading[0], tri.shading[1]);
        
        // Interpolate to find shading value at right end
        endShading = lerp(tri.X[1], tri.Y[1], tri.X[2], tri.Y[2], endX, row, tri.shading[1], tri.shading[2]);

#if DEBUG > 2
        printf("startX: %.16f\n", startX);
        printf("endX: %.16f\n", endX);
        printf("startZ: %.16f\n", startZ);
        printf("endZ: %.16f\n", endZ);
        printf("startXColor: [%.3f, %.3f, %.3f]\n", startXColor[0], startXColor[1], startXColor[2]);
        printf("endXColor: [%.3f, %.3f, %.3f]\n", endXColor[0], endXColor[1], endXColor[2]);
        printf("startShading: %.16f\n", startShading);
        printf("endShading: %.16f\n", endShading);
#endif

        // For each column
        for (int col = ceil441(startX); col <= floor441(endX); ++col)
        {
            // Interpolate to find current z value
            z = lerp(startX, row, endX, row, col, row, startZ, endZ);

            // If z > current z
            if (z > screen.getZ(col, row))
            {
                
                // Interpolate to find current shading value
                shading = lerp(startX, row, endX, row, col, row, startShading, endShading);

                // Interpolate to find color at this pixel
                for (int i = 0; i < NUM_COLORS; ++i)
                {
                    color[i] = lerp(startX, row, endX, row, col, row, startXColor[i], endXColor[i]);
                }

#if DEBUG > 3
                printf("Interpolated color: [%.2f, %.2f, %.2f]\n", color[0], color[1], color[2]);
                printf("Interpolated z: %.4f\n", z);
                printf("Interpolated shading: %.4f\n", shading);
#endif
                // Set the pixel at the current row and column
                screen.setPixel(col, row, ceil441(255 * std::min(1.0, color[RED_INDEX] * shading)),
                                          ceil441(255 * std::min(1.0, color[GREEN_INDEX] * shading)),
                                          ceil441(255 * std::min(1.0, color[BLUE_INDEX] * shading)), z);
            }
        }
    }
}

int scanline(std::vector<Triangle>& triangles, Matrix& m, double position[], Screen& screen)
{
    double viewDirection[DIMENSIONS];
    std::vector<Triangle> toProcess;

    // Scanline algorithm:

    // For each triangle
    for (std::vector<Triangle>::iterator it = triangles.begin(); it != triangles.end(); ++it)
    {
        toProcess.clear();

        // Calculate the shading for the triangle
        for (int i = 0; i < POINTS_IN_TRIANGLE; ++i)
        {
            //  triangle vertex minus camera position
            viewDirection[0] = it->X[i];
            viewDirection[1] = it->Y[i];
            viewDirection[2] = it->Z[i];
            subtract(viewDirection, position, viewDirection);
            normalize(viewDirection);

            //  then call CalculateShading with correct viewDir
            //  The angle between the normal and the light source should still be valid
            it->shading[i] = calculateShading(lp, viewDirection, it->normals[i]);
        }

        // Create a copy so the original triangle is not altered and can be 
        // transformed again later.
        Triangle cpy(*it);

        // Transform the triangle
        transformTriangle(cpy, m);

        Triangle tri2;

        // If the triangle does not have either a flat bottom or a flat top
        if ((cpy.Y[0] != cpy.Y[1])
                && (cpy.Y[2] != cpy.Y[1]) 
                && (cpy.Y[0] != cpy.Y[2]))
        {
            // split the triangle into two
            splitTriangle(cpy, tri2);
            toProcess.push_back(tri2);
        }
        toProcess.push_back(cpy);

        for (std::vector<Triangle>::iterator tri = toProcess.begin(); tri != toProcess.end(); ++tri)
        {
            // Process the triangle
            tri->arrange();
            scanTriangle(*tri, m, position, screen);
        }
    }

    return 0;
}

int main()
{
    int loc = 0;

    // Read the triangles from the file
    std::vector<Triangle> triangles = GetTriangles();

    // Create the camera object and transformation matrices
    Camera camera = GetCamera(0, 1000);
    Matrix cameraTransform;
    Matrix viewTransform;
    Matrix deviceTransform;


    // World space to camera space
    camera.CameraTransform(cameraTransform);
    printf("Camera transform: \n");
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            printf("%.2f ", cameraTransform.A[i][j]);
        }
        printf("\n");
    }

    // Camera space to image space
    camera.ViewTransform(viewTransform);
    printf("View transform: \n");
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            printf("%.2f ", viewTransform.A[i][j]);
        }
        printf("\n");
    }

    // Image space to device space
    camera.DeviceTransform(deviceTransform);
    printf("Device transform: \n");
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            printf("%.2f ", deviceTransform.A[i][j]);
        }
        printf("\n");
    }

    Matrix A = Matrix::ComposeMatrices(cameraTransform, viewTransform);
    A = Matrix::ComposeMatrices(A, deviceTransform);
    printf("Composition: \n");
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            printf("%.2f ", A.A[i][j]);
        }
        printf("\n");
    }

    // Create the screen object
    Screen screen(WIDTH, HEIGHT);

    // Run the scanline algorithm
    scanline(triangles, A, camera.position, screen);

    // Save the image
    screen.writeImage(OUTPUT_FILENAME); std::cout << "Done. Image saved to " << OUTPUT_FILENAME << ".png" << std::endl;
}

